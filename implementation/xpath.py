import json
import codecs
import lxml.html


def remove_newlines_and_tabs(webpage_string):
    webpage_string = webpage_string.replace('\n', ' ')
    webpage_string = webpage_string.replace('\t', ' ')
    webpage_string = webpage_string.replace('\r', ' ')
    return webpage_string


def xpath_overstock(web_page_path):
    f = codecs.open(web_page_path, 'r')
    web_page_content = f.read()

    web_page_content = remove_newlines_and_tabs(web_page_content)
    parsed = lxml.html.fromstring(web_page_content)

    titles = parsed.xpath('/html/body/table/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/a/b/text()')

    list_prices = parsed.xpath('/html/body/table/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr[1]/td[2]/s/text()')
    # print(list_prices)
    prices = parsed.xpath('/html/body/table/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr[2]/td[2]/span/b/text()')
    # print(prices)
    savings_and_percents = parsed.xpath(
        '/html/body/table/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr[3]/td[2]/span/text()')
    # print(savings_and_percents)
    contents = parsed.xpath(
        '/html/body/table/tbody/tr[1]/td[5]/table/tbody/tr[2]/td/table/tbody/tr/td/table/tbody/tr/td[2]/table/tbody/tr/td[2]/span/text()')
    # print(len(contents))

    savings = []
    saving_percents = []
    for saving_and_percent in savings_and_percents:
        saving, percent = saving_and_percent.split(' ')
        savings.append(saving)
        percent = percent.replace('(', '')
        percent = percent.replace(')', '')
        saving_percents.append(percent)

    n = len(titles)
    data_items = []
    for i in range(n):
        data_item = {
            'Title': titles[i].strip(),
            'ListPrice': list_prices[i].strip(),
            'Price': prices[i].strip(),
            'Saving': savings[i].strip(),
            'SavingPercent': saving_percents[i].strip(),
            'Content': contents[i].strip()
        }
        data_items.append(data_item)
    extracted_data = {'dataItems': data_items}
    json_extracted_data = json.dumps(extracted_data, indent=4)
    print(json_extracted_data)


def xpath_rtvslo(web_page_path):
    f = codecs.open(web_page_path, 'r', 'utf-8')  # --> ce ni utf8, pol rtvslo nima sumnikov... ce je utf8 pol overstock ne dela
    web_page_content = f.read()

    web_page_content = remove_newlines_and_tabs(web_page_content)

    parsed = lxml.html.fromstring(web_page_content)
    author_name = parsed.xpath('/html/body/div[@id="main-container"]/div[contains(@class, "news-container")]/div[@class="row"]/div[@class="article-meta"]/div[@class="author"]/div/text()')
    # print(author_name)
    published_time = parsed.xpath('/html/body/div[@id="main-container"]/div[contains(@class, "news-container")]/div[@class="row"]/div[@class="article-meta"]/div[@class="publish-meta"]/text()')
    # print(published_time)
    title = parsed.xpath('/html/body/div[@id="main-container"]/div[contains(@class, "news-container")]/div[@class="row"]/header/h1/text()')
    # print(title)
    subtitle = parsed.xpath(
        '/html/body/div[@id="main-container"]/div[contains(@class, "news-container")]/div[@class="row"]/header/div[@class="subtitle"]/text()')
    # print(subtitle)
    lead = parsed.xpath(
        '/html/body/div[@id="main-container"]/div[contains(@class, "news-container")]/div[@class="row"]/header/p[@class="lead"]/text()')
    # print(lead)
    content = parsed.xpath(
        '/html/body/div[@id="main-container"]/div[contains(@class, "news-container")]/div[@class="row"]/div[@class="article-body"]/article/p/text() | /html/body/div[@id="main-container"]/div[contains(@class, "news-container")]/div[@class="row"]/div[@class="article-body"]/article/p/strong/text()')
    # print(content)

    # join contents
    joined_content = ' '.join(content)

    n = 1  # one data record
    data_items = []
    for i in range(n):
        data_item = {
            'Author': author_name[i].strip(),
            'PublishedTime': published_time[i].strip(),
            'Title': title[i].strip(),
            'SubTitle': subtitle[i].strip(),
            'Lead': lead[i].strip(),
            'Content': joined_content.strip()
        }
        data_items.append(data_item)
    extracted_data = {'dataItems': data_items}
    json_extracted_data = json.dumps(extracted_data, indent=4)
    print(json.loads(json_extracted_data))  # nujno loads, drugače ne zna šumnikov prikazat


def xpath_custom(web_page_path):
    f = codecs.open(web_page_path, 'r')
    web_page_content = f.read()
    web_page_content = remove_newlines_and_tabs(web_page_content)
    parsed = lxml.html.fromstring(web_page_content)

    title = parsed.xpath('/html/body/div[@id="page"]/div[@id="content"]/div/div[@id="primary"]/main/div/div[contains(@class, "summary")]/h1/text()')
    # print(title)
    price = parsed.xpath(
        '/html/body/div[@id="page"]/div[@id="content"]/div/div[@id="primary"]/main/div/div[contains(@class, "summary")]/p/span/text() | /html/body/div[@id="page"]/div[@id="content"]/div/div[@id="primary"]/main/div/div[contains(@class, "summary")]/p/span/span/text()')
    # print(price)
    short_description = parsed.xpath(
        '/html/body/div[@id="page"]/div[@id="content"]/div/div[@id="primary"]/main/div/div[contains(@class, "summary")]/div[1]/p/text()')
    # print(short_description)
    stock_keeping_unit = parsed.xpath(
        '/html/body/div[@id="page"]/div[@id="content"]/div/div[@id="primary"]/main/div/div[contains(@class, "summary")]/div[2]/span/span/text()')
    # print(stock_keeping_unit)
    category = parsed.xpath(  # //div[contains(@class, "summary")]/div[2]/span/a/text() <--- WORKS TOO
        '/html/body/div[@id="page"]/div[@id="content"]/div/div[@id="primary"]/main/div/div[contains(@class, "summary")]/div[2]/span/a/text()')
    # print(category)
    long_description = parsed.xpath(
        '/html/body/div[@id="page"]/div[@id="content"]/div/div[@id="primary"]/main/div/div[3]/div[1]/p/text()')
    # print(long_description)
    related_products = parsed.xpath(
        '//main/div/section/ul/li/a/h2/text()')
    # print(related_products)

    joined_price = ''.join(price)
    joined_sku = ', '.join(stock_keeping_unit)
    joined_category = ', '.join(category)
    joined_long_description = ' '.join(long_description)
    joined_related_products = ', '.join(related_products)

    n = 1  # one data record
    data_items = []
    for i in range(n):
        data_item = {
            'Title': title[i].strip(),
            'Price': joined_price.strip(),
            'ShortDescription': short_description[i].strip(),
            'StockKeepingUnit': joined_sku.strip(),
            'Category': joined_category.strip(),
            'LongDescription': joined_long_description.strip(),
            'RelatedProducts': joined_related_products.strip()
        }
        data_items.append(data_item)
    extracted_data = {'dataItems': data_items}
    json_extracted_data = json.dumps(extracted_data, indent=4)
    print(json.loads(json_extracted_data))  # brez tega ne narise pravilno funta


if __name__ == '__main__':
    # overstock
    xpath_overstock('../input/overstock.com/jewelry01.html')
    xpath_overstock('../input/overstock.com/jewelry02.html')

    # rtvslo
    xpath_rtvslo('../input/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html')
    xpath_rtvslo('../input/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljše v razredu - RTVSLO.si.html')

    # our own two similar web pages
    xpath_custom('../input/themes.woocommerce.com/knife-set.html')
    xpath_custom('../input/themes.woocommerce.com/blouse.html')
