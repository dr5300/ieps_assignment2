import re
import json
import codecs


def regular_expressions_overstock(web_page_path):
    f = codecs.open(web_page_path, 'r')
    web_page_content = f.read()
    titles = re.findall(r'<a .*><b>(.+)</b></a><br>', web_page_content)
    list_prices = re.findall(r'List Price:.*<s>(.+)</s>', web_page_content)
    prices = re.findall(r'Price:.*<b>(.+)</b>', web_page_content)
    savings = re.findall(r'You Save:.*<span .*>(.+) .*</span>', web_page_content)
    saving_percents = re.findall(r'You Save:.*<span .*>.* (.+)</span>', web_page_content)
    saving_percents = list(map(lambda p: p.replace('(', '').replace(')', ''), saving_percents))
    contents = re.findall(r'(?:<span class="normal">((?:.*?\r?\n?)*)<br>)+<a.*Click here to purchase.', web_page_content)

    n = len(titles)
    data_items = []
    for i in range(n):
        data_item = {
            'Title': titles[i].replace('\n', ' ').strip(),
            'ListPrice': list_prices[i].replace('\n', ' ').strip(),
            'Price': prices[i].replace('\n', ' ').strip(),
            'Saving': savings[i].replace('\n', ' ').strip(),
            'SavingPercent': saving_percents[i].replace('\n', ' ').strip(),
            'Content': contents[i].replace('\n', ' ').strip()
        }
        data_items.append(data_item)
    extracted_data = {'dataItems': data_items}
    json_extracted_data = json.dumps(extracted_data, indent=4)
    print(json_extracted_data)


def remove_newlines_and_tabs(webpage_string):
    webpage_string = webpage_string.replace('\n', ' ')
    webpage_string = webpage_string.replace('\t', ' ')
    webpage_string = webpage_string.replace('\r', ' ')
    return webpage_string


def regular_expressions_rtvslo(web_page_path):
    f = codecs.open(web_page_path, 'r', 'utf-8')  # utff8 --> sumniki
    web_page_content = f.read()
    web_page_content = remove_newlines_and_tabs(web_page_content)
    author_name = re.findall(r'<div class="author-name">(.*?)</div>', web_page_content)
    #print(author_name)
    published_time = re.findall(r'<div class="publish-meta">\s*(.*?)<br>', web_page_content)
    #print(published_time)
    title = re.findall(r'<h1>(.*)</h1>', web_page_content)
    #print(title)
    subtitle = re.findall(r'<div class="subtitle">(.*?)</div>', web_page_content)
    #print(subtitle)
    lead = re.findall(r'<p class="lead">(.*?)</p>', web_page_content)
    #print(lead)
    content = re.findall(r'<article class="article">(.*?)<div class="gallery">', web_page_content)
    # only one string is in content
    content = content[0]
    # remove html tags from content
    content = re.findall(r'<p.*?>(.*?)</p>', content)

    # join contents
    joined_content = ' '.join(content)

    # remove html tags
    joined_content_without_html_tags = re.sub(r'<.*?>', '', joined_content)
    joined_content_without_html_tags = joined_content_without_html_tags.strip()

    n = 1  # one data record
    data_items = []
    for i in range(n):
        data_item = {
            'Author': author_name[i].strip(),
            'PublishedTime': published_time[i].strip(),
            'Title': title[i].strip(),
            'SubTitle': subtitle[i].strip(),
            'Lead': lead[i].strip(),
            'Content': joined_content_without_html_tags.strip()
        }
        data_items.append(data_item)
    extracted_data = {'dataItems': data_items}
    json_extracted_data = json.dumps(extracted_data, indent=4)
    print(json.loads(json_extracted_data))  # nujno loads, drugače ne zna šumnikov prikazat


def regular_expressions_custom(web_page_path):
    f = codecs.open(web_page_path, 'r', 'utf-8')
    web_page_content = f.read()
    web_page_content = remove_newlines_and_tabs(web_page_content)

    title = re.findall(r'<h1 class="product_title.*?>(.*?)</h1>', web_page_content)
    #print(title)
    price = re.findall(r'<p class="price"><span.*?><span.*?>(.*?)</span>(.*?)</span>', web_page_content)
    #print(price)
    short_description = re.findall(r'short-description.*?>\s*<p>(.*?)</p>', web_page_content)
    #print(short_description)
    stock_keeping_unit = re.findall(r'SKU:.*?>(.*?)<', web_page_content)
    #print(stock_keeping_unit)
    category = re.findall(r'Categor[^<]*?<a[^>]*?>([^<]*?)<', web_page_content)
    #print(category)
    long_description = re.findall(r'<h2>Description</h2>.*?<p>(.*?)</p>', web_page_content)
    #print(long_description)
    related_products = re.findall(r'<li class="post.*?<h2.*?>(.*?)</h2>', web_page_content, flags=re.U)
    #print(related_products)

    joined_price = ''.join(price[0])  # price is tuple
    joined_sku = ', '.join(stock_keeping_unit)
    joined_category = ', '.join(category)
    joined_long_description = ' '.join(long_description)
    joined_related_products = ', '.join(related_products)

    n = 1  # one data record
    data_items = []
    for i in range(n):
        data_item = {
            'Title': title[i].strip(),
            'Price': joined_price.strip(),
            'ShortDescription': short_description[i].strip(),
            'StockKeepingUnit': joined_sku.strip(),
            'Category': joined_category.strip(),
            'LongDescription': joined_long_description.strip(),
            'RelatedProducts': joined_related_products.strip()
        }
        data_items.append(data_item)
    extracted_data = {'dataItems': data_items}
    json_extracted_data = json.dumps(extracted_data, indent=4)
    print(json_extracted_data)


if __name__ == '__main__':
    # overstock
    regular_expressions_overstock('../input/overstock.com/jewelry01.html')
    regular_expressions_overstock('../input/overstock.com/jewelry02.html')

    # rtvslo
    regular_expressions_rtvslo('../input/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html')
    regular_expressions_rtvslo('../input/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljše v razredu - RTVSLO.si.html')

    # our own two similar web pages
    regular_expressions_custom('../input/themes.woocommerce.com/knife-set.html')
    regular_expressions_custom('../input/themes.woocommerce.com/blouse.html')
