from bs4 import BeautifulSoup
import codecs
import re
from enum import Enum


class LineType(Enum):
    TAG_OPEN = 1
    TAG_CLOSE = 2
    TAG_OPEN_CLOSE = 3
    CONTENT = 4


def preprocessing(web_page_path):
    f = codecs.open(web_page_path, 'r')
    web_page_content = f.read()
    bs = BeautifulSoup(web_page_content, 'lxml')

    # remove script elements
    for sc in bs('script'):
        sc.extract()
    for ns in bs('noscript'):
        ns.extract()
    # remove style elements
    for st in bs('style'):
        st.extract()
    # remove link elements
    for l in bs('link'):
        l.extract()
    # remove meta elements
    for m in bs('meta'):
        m.extract()
    # remove comments
    web_page_content = re.sub('(<!--.*?-->)', '', str(bs), flags=re.DOTALL)
    # remove doctype
    web_page_content = web_page_content.replace('<!DOCTYPE html>', '')

    tags = re.findall(r'(<.*?>)', web_page_content, flags=re.DOTALL)

    s = web_page_content
    new_line_arranged_s = ''
    for i in range(len(tags) - 1):
        tag_a = tags[i]
        tag_b = tags[i + 1]

        ta_i = s.find(tag_a)

        sb = s[ta_i+len(tag_a):]
        tb_i = ta_i + len(tag_a) + sb.find(tag_b)

        start = ta_i + len(tag_a)
        end = tb_i
        content_between_tags = s[start:end].strip().replace('\n', ' ')
        new_line_arranged_s += tag_a + '\n'
        if content_between_tags:
            new_line_arranged_s += content_between_tags + '\n'
        s = s[tb_i:]
    new_line_arranged_s += tags[-1]

    s = new_line_arranged_s.splitlines()
    tab_arranged_s = ''
    tab_count = 0
    for line in s:
        line_type = get_line_type(line)
        new_line = ''

        if line_type == LineType.TAG_CLOSE:
            tab_count -= 1

        for i in range(tab_count):
            new_line += '\t'
        new_line += line + '\n'
        tab_arranged_s += new_line

        if line_type == LineType.TAG_OPEN:
            tab_count += 1

    return tab_arranged_s


def get_line_type(line):
    if '</' in line:
        return LineType.TAG_CLOSE
    elif '/>' in line:
        return LineType.TAG_OPEN_CLOSE
    elif '<' in line:
        return LineType.TAG_OPEN
    else:
        return LineType.CONTENT


def is_tag(line):
    line_type = get_line_type(line)
    return line_type != LineType.CONTENT


def get_tag_type(line):
    line_type = get_line_type(line)
    if line_type == LineType.TAG_OPEN or line_type == LineType.TAG_OPEN_CLOSE:
        return line.strip().replace('<', '').split()[0]
    elif line_type == LineType.TAG_CLOSE:
        return line.strip().replace('</', '').split()[0]


def get_indent_level(line):
    return line.count('\t')


def tag_mismatch(line_a, line_b):
    if is_tag(line_a) and is_tag(line_b):
        if get_tag_type(line_a) != get_tag_type(line_b):
           return True
    return False


def string_mismatch(line_a, line_b):
    if get_line_type(line_a) == get_line_type(line_b):
        if line_a != line_b:
            return True
    return False


def string_tag_mismatch(line_a, line_b):
    if (is_tag(line_a) and get_line_type(line_b) == LineType.CONTENT) or (is_tag(line_b) and get_line_type(line_a) == LineType.CONTENT):
        return True
    return False


def get_element_lines(i, lines):
    if get_line_type(lines[i]) == LineType.TAG_OPEN_CLOSE:
        return [lines[i]]
    elif get_line_type(lines[i]) == LineType.TAG_OPEN:
        start_line = lines[i]
        start_line_indent_level = get_indent_level(start_line)
        element_lines = [start_line]
        i += 1
        while get_indent_level(lines[i]) > start_line_indent_level:
            element_lines.append(lines[i])
            i += 1
        element_lines.append(lines[i])
        return element_lines


def road_runner(web_page_path_a, web_page_path_b):
    web_page_a = preprocessing(web_page_path_a)
    web_page_b = preprocessing(web_page_path_b)

    web_page_a_lines = web_page_a.splitlines()
    web_page_b_lines = web_page_b.splitlines()

    n_a = len(web_page_a_lines)
    n_b = len(web_page_b_lines)
    i_a = 0
    i_b = 0
    wrapper_lines = []
    while i_a < n_a and i_b < n_b:
        line_a = web_page_a_lines[i_a]
        line_a_type = get_line_type(line_a)
        line_a_indent_level = get_indent_level(line_a)

        line_b = web_page_b_lines[i_b]
        line_b_type = get_line_type(line_b)
        line_b_indent_level = get_indent_level(line_b)

        if line_a != line_b:
            if tag_mismatch(line_a, line_b):
                missing_lines = []
                if line_a_indent_level <= line_b_indent_level:
                    missing_lines = get_element_lines(i_b, web_page_b_lines)
                    i_b += len(missing_lines)
                else:
                    missing_lines = get_element_lines(i_a, web_page_a_lines)
                    i_a += len(missing_lines)

                """
                wrapper_lines.append('(')
                wrapper_lines += missing_lines
                wrapper_lines.append(')?')
                """

                missing_line_indent_level = get_indent_level(missing_lines[0])
                missing_line = '(' + missing_lines[0].strip()
                for i in range(missing_line_indent_level):
                    missing_line = '\t' + missing_line
                missing_lines[0] = missing_line
                missing_line_indent_level = get_indent_level(missing_lines[-1])
                missing_line = missing_lines[-1].strip() + ')?'
                for i in range(missing_line_indent_level):
                    missing_line = '\t' + missing_line
                missing_lines[-1] = missing_line
                wrapper_lines += missing_lines

                continue

            elif string_tag_mismatch(line_a, line_b):
                missing_line = ''
                if get_line_type(line_a) == LineType.CONTENT:
                    missing_line = line_a
                    i_a += 1
                else:
                    missing_line = line_b
                    i_b += 1

                missing_line_indent_level = get_indent_level(missing_line)
                missing_line = '(' + missing_line.strip() + ')?'
                for i in range(missing_line_indent_level):
                    missing_line = '\t' + missing_line
                wrapper_lines.append(missing_line)
                """
                wrapper_lines.append('(')
                wrapper_lines.append(missing_line)
                wrapper_lines.append(')?')
                """
                continue

            elif string_mismatch(line_a, line_b):
                left_string_match = ''
                for i in range(len(line_a)):
                    i_c_a = line_a[i]
                    i_c_b = line_b[i]
                    if i_c_a == i_c_b:
                        left_string_match = left_string_match + i_c_a
                    else:
                        break
                right_string_match = ''
                for i in range(len(line_a)):
                    i_c_a = line_a[-(i + 1)]
                    i_c_b = line_b[-(i + 1)]
                    if i_c_a == i_c_b:
                        right_string_match = i_c_a + right_string_match
                    else:
                        break
                wrapper_lines.append(left_string_match + '(.*)' + right_string_match)
                i_a += 1
                i_b += 1
                continue
            else:
                print('Not defined mismatch')

        wrapper_lines.append(line_a)
        i_a += 1
        i_b += 1

    for line in wrapper_lines:
        print(line)



if __name__ == '__main__':

    # overstock
    overstock_web_page_path_a = '../input/overstock.com/jewelry01.html'
    overstock_web_page_path_b = '../input/overstock.com/jewelry02.html'
    road_runner(overstock_web_page_path_a, overstock_web_page_path_b)

    # rtvslo
    rtvslo_web_page_path_a = '../input/rtvslo.si/Audi A6 50 TDI quattro_ nemir v premijskem razredu - RTVSLO.si.html'
    rtvslo_web_page_path_b = '../input/rtvslo.si/Volvo XC 40 D4 AWD momentum_ suvereno med najboljše v razredu - RTVSLO.si.html'
    road_runner(rtvslo_web_page_path_a, rtvslo_web_page_path_b)

    # woocommerce
    woocommerce_web_page_path_a = '../input/themes.woocommerce.com/knife-set.html'
    woocommerce_web_page_path_b = '../input/themes.woocommerce.com/blouse.html'
    road_runner(woocommerce_web_page_path_a, woocommerce_web_page_path_b)
