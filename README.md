# IEPS - Programming assignment 2

## Installation

Use *pip* to install required packages:

```bash
pip install -r requirements.txt
```

## Usage

### Regular expressions implementation
To use regular expressions for extracting data run *regular_expressions.py* inside implementation folder.

### XPath implementation
To use XPath for extracting data run *xpath.py* inside implementation folder.

### RoadRunner-like implementation
To use RoadRunner for getting wrappers for data extraction run *road_runner.py* inside implementation folder.

### Contributors ###
* **Denis Rajković**
* **Matic Bizjak**